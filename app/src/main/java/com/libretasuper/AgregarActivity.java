package com.libretasuper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.libretasuper.database.DBHandler;
import com.libretasuper.modelo.Categoria;
import com.libretasuper.modelo.Item;

import java.util.ArrayList;

public class AgregarActivity extends AppCompatActivity {
    //var
    private ArrayList<Categoria> lstCategorias=new ArrayList<>();
    private ArrayList<Item> lstItems=new ArrayList<>();
    private Categoria categoriaSelect;
    private Item itemSelect;
    private DBHandler gestorDB;
    //view
    Spinner spCategorias,spItems;
    EditText txtCantidad;
    //*****************************
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar);
        //----------------------------------------
        Toolbar toolbar_agregar= (Toolbar)findViewById(R.id.toolbar_agregar);
        setSupportActionBar(toolbar_agregar);
        //----------------------------------------
        spCategorias=(Spinner)findViewById(R.id.spCategoria);
        spItems=(Spinner)findViewById(R.id.spItem);
        txtCantidad=(EditText)findViewById(R.id.txtCantidad);
        //----------------------------------------
        gestorDB=new DBHandler(this);
        //----------------------------------------
        loadCategorias();
        txtCantidad.append("");
    }
    //**************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_agregar,menu);
        return true;
       // return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.salir_agregar:
                cerrarActivity();
                break;
            default: return super.onOptionsItemSelected(item);
        }
        return true;
    }

    //**************************************
    private void loadCategorias(){

        lstCategorias=gestorDB.listaCategorias();

        loadSpCategorias();
    }

    private void loadSpCategorias(){

        ArrayAdapter adapter=new ArrayAdapter<Categoria>(
                getApplicationContext(),
                R.layout.my_spinner,
                lstCategorias
        );
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        spCategorias.setAdapter(adapter);

        spCategorias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                categoriaSelect=(Categoria)adapterView.getItemAtPosition(i);
                loadItems();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    private void loadItems(){

       lstItems.clear();
       lstItems=this.getItemsFromCategoria(categoriaSelect);
       loadSpItems();
    }

    private ArrayList<Item> getItemsFromCategoria(Categoria categoria){
    //here it'll search on database for items in that category
        ArrayList<Item> result=new ArrayList<>();
        //---------------------------
        result=gestorDB.listaItemsFromCategoria(categoria);
        //---------------------------

        return result;
    }
    private void loadSpItems(){
        ArrayAdapter adapter=new ArrayAdapter<Item>(
                getApplicationContext(),
                R.layout.my_spinner,
                lstItems
        );
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spItems.setAdapter(adapter);

        spItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                itemSelect=(Item) adapterView.getItemAtPosition(i);
                txtCantidad.setText(String.valueOf(itemSelect.getCantidad()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void agregarItemLista(View view){
        boolean result=false;
        int nuevaCantidad=Integer.parseInt(txtCantidad.getText().toString());

        if(nuevaCantidad>0 && itemSelect!=null){
            itemSelect.setCantidad(nuevaCantidad);
            result=gestorDB.addItemToList(itemSelect);
        }
        String mensaje=(result)?"Se agrego item":"No se agrego el item";
        Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show();
    }

    public void btnInc(View view){
        int cantidad=Integer.parseInt(txtCantidad.getText().toString());
        cantidad++;
        txtCantidad.setText(String.valueOf(cantidad));
    }
    public void btnDec(View view){
        int cantidad=Integer.parseInt(txtCantidad.getText().toString());
        cantidad--;
        if(cantidad >= 0)
            txtCantidad.setText(String.valueOf(cantidad));
    }

    public void cerrarActivity(){
        setResult(1);
        finish();
    }
}//end
