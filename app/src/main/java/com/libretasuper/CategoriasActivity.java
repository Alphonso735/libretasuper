package com.libretasuper;


import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.libretasuper.adapters.AdapterListaCategorias;
import com.libretasuper.database.DBHandler;
import com.libretasuper.modelo.Categoria;

import java.util.ArrayList;

public class CategoriasActivity extends AppCompatActivity {
    //var
    private DBHandler gestorDB;
    private ArrayList<Categoria> listaCategorias=new ArrayList<>();
    private Categoria categoriaSelec=null;
    //--------------------------
    private EditText txtNuevoCat;
    private Button btnAgregarCat;
    private View viewSelect=null;
    private RecyclerView recyclerCategorias;
    //--------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categorias);
        //-------------------------------------
        txtNuevoCat=(EditText)findViewById(R.id.txtNuevoCat);
        btnAgregarCat=(Button)findViewById(R.id.btnAgregarCat);
        recyclerCategorias=(RecyclerView)findViewById(R.id.recyclercategoria);
        recyclerCategorias.setLayoutManager(new LinearLayoutManager(this));
        //-------------------------------------
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar_categorias);
        setSupportActionBar(toolbar);
        //-------------------------------------
        gestorDB=new DBHandler(this);
        //-------------------------------------
       habilitarCampos(false);
       cargarListadoCat();

    }
    //*************************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cat,menu);
        // return super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

       switch (item.getItemId()){
           case R.id.btnNuevaCat:
               btnNuevaCategoria();
               break;
           case R.id.btnBorrarCat:
               borrarCategoria();
               break;
           case R.id.salir_cat:
               cerrar();
               break;
           default:
                return super.onOptionsItemSelected(item);
       }
       return true;
    }

    //*************************************************

    public void btnNuevaCategoria(){
        boolean opcion=txtNuevoCat.isEnabled();

        habilitarCampos(!opcion);

    }

    public void agregarCategoria(View view){

        if(validarCampos()){

            Categoria nuevaCat=new Categoria(txtNuevoCat.getText().toString());

            if(gestorDB.insertarCategoria(nuevaCat)){
                cargarListadoCat();
                Toast.makeText(this,"Se agrego la categoria",Toast.LENGTH_LONG).show();
                txtNuevoCat.setText("");
            }
            else
                Toast.makeText(this,"Ya existe la categoria",Toast.LENGTH_LONG).show();


        }else{
            Toast.makeText(this,"Error en el nombre de la categoria",Toast.LENGTH_LONG).show();
        }
    }

    public void cerrar(){
        finish();
    }

    //***********************************************
    private void cargarListadoCat(){

        listaCategorias.clear();
        listaCategorias=gestorDB.listaCategorias();
        //------------------

        cargarRecycler();
    }

    private void cargarRecycler(){

        AdapterListaCategorias adapter=new AdapterListaCategorias(listaCategorias);

        recyclerCategorias.setAdapter(adapter);

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View viewActual=recyclerCategorias.getChildViewHolder(view).
                        itemView.findViewById(R.id.layout_lista_categorias);

                if(viewSelect==null){
                    viewSelect=viewActual;
                    viewSelect.setBackgroundColor(Color.RED);
                }else{
                    if(viewActual!=viewSelect){
                        viewSelect.setBackgroundColor(Color.WHITE);
                        viewActual.setBackgroundColor(Color.RED);
                        viewSelect=viewActual;
                    }
                }
                //--------------------
                categoriaSelec=listaCategorias.get(recyclerCategorias.getChildAdapterPosition(view));
            }
        });
    }

    public void borrarCategoria(){
        if(categoriaSelec!=null){
            String msg=(gestorDB.borrarCategoria(categoriaSelec))?"Se borro la categoria":"No se borro la categoria";

            Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this,"Seleccione una categoria antes",Toast.LENGTH_SHORT).show();
        }
        this.cargarListadoCat();
    }

    private boolean validarCampos(){
        String catNombre;
        catNombre = txtNuevoCat.getText().toString();

        return !"".equalsIgnoreCase(catNombre);
    }

    private void habilitarCampos(boolean opcion){
        txtNuevoCat.setEnabled(opcion);
        btnAgregarCat.setEnabled(opcion);
    }

}//end
