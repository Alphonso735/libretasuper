package com.libretasuper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.libretasuper.database.DBHandler;
import com.libretasuper.modelo.Categoria;

public class ConfiguracionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);
        //----------------------------
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar_config);
        setSupportActionBar(toolbar);
        //----------------------------

    }
    //****************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_config,menu);
        //return super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

            switch (item.getItemId()){
                case R.id.salir_config:
                    cerrar();
                    break;
                 default:return super.onOptionsItemSelected(item);
            }
            return true;
    }

    //****************************************

    public void callCategoriaActivity(View view){
        Intent intent=new Intent(this, CategoriasActivity.class);

        startActivity(intent);
    }

    public void callItemActivity(View view){
        Intent intent=new Intent(this,ItemsActivity.class);


        startActivity(intent);
    }
    public void cerrar(){
        finish();
    }

    public void resetear(View view){
        DBHandler gestorDB=new DBHandler(this);
        gestorDB.resetearBD();
        Toast.makeText(this, "Se reseto la app.", Toast.LENGTH_SHORT).show();
    }
}//end
