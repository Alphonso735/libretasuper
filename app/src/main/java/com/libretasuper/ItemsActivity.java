package com.libretasuper;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.libretasuper.adapters.AdapterListaItems;
import com.libretasuper.database.DBHandler;
import com.libretasuper.modelo.Categoria;
import com.libretasuper.modelo.Item;

import java.util.ArrayList;

public class ItemsActivity extends AppCompatActivity {
    //var
    private DBHandler gestorDB;
    private ArrayList<Categoria> listaCategorias=new ArrayList<>();
    private ArrayList<Item> listaItems=new ArrayList<>();
    private Categoria categoriaSelect=null;
    private Item itemSelect=null;
    //------------------------
    private EditText txtNuevoItem;
    private Button btnAgregarItem;
    private Spinner spCatItem;
    private RecyclerView recyclerItem;
    private View viewSelect=null;

    //------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);
        //----------------------------
        txtNuevoItem=(EditText)findViewById(R.id.txtNuevoItem);

        btnAgregarItem=(Button)findViewById(R.id.btnAgregarItem);
        spCatItem=(Spinner)findViewById(R.id.spCatItem);
        recyclerItem=(RecyclerView)findViewById(R.id.recyclerItem);
        recyclerItem.setLayoutManager(new LinearLayoutManager(this));
        //----------------------------
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar_items);
        setSupportActionBar(toolbar);
        //----------------------------
        gestorDB=new DBHandler(this);
        //-----------------------------
        habilitarCampos(false);
        cargarSpCategorias();
        cargarListadoItems();
    }
    //******************************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_items,menu);
        return true;
        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.btnNuevoItem:
                nuevoItem();
                break;
            case R.id.btnBorrarItem:
                borrarItem();
                break;
            case R.id.salir_item:
                cerrar();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
    //******************************************************

    public void nuevoItem(){
        boolean opcion=txtNuevoItem.isEnabled();

        habilitarCampos(!opcion);
    }

    public void agregarItem(View view){
        if(validarCampos() && categoriaSelect!=null){

            Item nuevoItem=new Item(txtNuevoItem.getText().toString(),
                    0,categoriaSelect);

            boolean result=gestorDB.insertarItem(nuevoItem);

            String respuesta=(result)?"Se agrego el item":"No se agrego el item";

            Toast.makeText(this,respuesta,Toast.LENGTH_LONG).show();

            if(result){
                cargarListadoItems();
                txtNuevoItem.setText("");
            }

        }else{
            Toast.makeText(this,"Error en los datos. Verifique",Toast.LENGTH_LONG).show();
        }
    }

    public void cerrar(){finish(); }

//*********************************************************
    private void cargarListadoItems(){
        listaItems.clear();

        listaItems=gestorDB.listaCompletaItems();

        cargarRecycler();
    }

    private void cargarRecycler(){
        AdapterListaItems adapter=new AdapterListaItems(listaItems);
        recyclerItem.setAdapter(adapter);
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View viewActual=recyclerItem.getChildViewHolder(view).
                        itemView.findViewById(R.id.layout_lista_items);

                if(viewSelect==null){
                    viewSelect=viewActual;
                    viewSelect.setBackgroundColor(Color.RED);
                }else{
                    if(viewActual!=viewSelect){
                        viewSelect.setBackgroundColor(Color.WHITE);
                        viewActual.setBackgroundColor(Color.RED);
                        viewSelect=viewActual;
                    }
                }
                //--------------------
                itemSelect=listaItems.get(recyclerItem.getChildAdapterPosition(view));
            }
        });
    }

    private boolean validarCampos(){
        String cadena=txtNuevoItem.getText().toString();
        return !"".equalsIgnoreCase(cadena);
    }

    private void cargarSpCategorias(){
        cargarListaCategorias();

        ArrayAdapter adapter=new ArrayAdapter<Categoria>(
                getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                listaCategorias
        );

        spCatItem.setAdapter(adapter);

        spCatItem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                categoriaSelect=(Categoria)adapterView.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    private void cargarListaCategorias(){
        listaCategorias.clear();
        listaCategorias=gestorDB.listaCategorias();
    }
    private void habilitarCampos(boolean opcion){
        txtNuevoItem.setEnabled(opcion);

        btnAgregarItem.setEnabled(opcion);
        spCatItem.setEnabled(opcion);
    }

    public void borrarItem(){
        if(itemSelect!=null){
            String msg=(gestorDB.borrarItem(itemSelect))?"Se borro el item":"No se pudo borrar el item";
            Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
            this.cargarListadoItems();
        }else{
            Toast.makeText(this,"Seleccione un item antes",Toast.LENGTH_SHORT).show();
        }
    }
}//end
