package com.libretasuper;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.widget.Toolbar;

import com.libretasuper.adapters.RecyclerViewAdapter;
import com.libretasuper.database.DBHandler;
import com.libretasuper.modelo.Item;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private RecyclerView recyclerView;
    //var
    private ArrayList<Item> listaItems=new ArrayList<>();
    private View viewSelect=null;
    private Item itemSelect=null;

    private DBHandler gestorDB;
    //------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: started");
        //----------------------------------
        android.support.v7.widget.Toolbar toolbar=(android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
       // toolbar.setPopupTheme(R.style.Theme_AppCompat_Light);
        setSupportActionBar(toolbar);
        //----------------------------------
       gestorDB=new DBHandler(this);
        //----------------------------------
        recyclerView=findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //-------------------------------
        getItems();
    }
    //*************************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        //return super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.agregar:
                callAgregarActivity();
                break;
            case R.id.restar:
                borrarItemLista();
                break;
            case R.id.configuracion:
                callConfiguracionActivity();
                break;
            case R.id.salir:
                cerrarApp();
                break;
            default:return super.onOptionsItemSelected(item);
        }
        return true;
    }

    //***********************************
    private void getItems(){
        Log.d(TAG, "getItems: Start");
        listaItems.clear();
     //*********************************
         listaItems=gestorDB.listaItems();
     //********************************
        initRecyclerView();
    }

    private void initRecyclerView(){
        Log.d(TAG, "initRecyclerView: init recycler");

        RecyclerViewAdapter adapter=new RecyclerViewAdapter(listaItems);
        //***********************************
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: in the recycler");
                    View viewActual=recyclerView.getChildViewHolder(view).itemView.findViewById(R.id.parent_layout);

                    if(viewSelect==null){
                        viewSelect=viewActual;
                        viewSelect.setBackgroundColor(Color.RED);

                    }else{
                        if(viewActual!=viewSelect){
                            viewSelect.setBackgroundColor(Color.WHITE);
                            viewActual.setBackgroundColor(Color.RED);
                            viewSelect=viewActual;
                        }
                    }
                //------------
                itemSelect=listaItems.get(recyclerView.getChildAdapterPosition(view));
            }
        });

        //***********************************
        recyclerView.setAdapter(adapter);

    }
    public void cerrarApp(){
        Log.d(TAG, "cerrarApp: close");
        gestorDB.cerrar();
        finish();
    }
    public void callAgregarActivity(){
        Log.d(TAG, "callAgregarActivity: start AgregarActivity");
        Intent intent=new Intent(this,AgregarActivity.class);
        startActivityForResult(intent,1);
    }

    public void callConfiguracionActivity(){
        Log.d(TAG, "callConfiguracionActivity: start configuracionActivity");
        Intent intent=new Intent(this,ConfiguracionActivity.class);
        startActivity(intent);
    }

    public void borrarItemLista(){
        boolean result=false;
        if(itemSelect!=null){
            result=gestorDB.borrarItemLista(itemSelect);

        }
        String txtResult=(result)?"Se borro el item":"No se pudo borrar el item";
        Toast.makeText(this,txtResult,Toast.LENGTH_LONG).show();

        getItems();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(TAG, "onActivityResult: end agregarActivity");
        super.onActivityResult(requestCode, resultCode, data);
        this.getItems();
    }
}//end
