package com.libretasuper.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.libretasuper.R;
import com.libretasuper.modelo.Categoria;

import java.util.ArrayList;

public class AdapterListaCategorias extends RecyclerView.Adapter<AdapterListaCategorias.ViewHolder>
        implements View.OnClickListener{
    //var
    ArrayList<Categoria> listaCategorias;

    View.OnClickListener listener;
    //--------------------

    public AdapterListaCategorias(ArrayList<Categoria> lista) {

        this.listaCategorias=lista;

    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lista_categorias,null,false);

        view.setOnClickListener(this);

        AdapterListaCategorias.ViewHolder holder= new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder,int position) {
        Categoria categoria=listaCategorias.get(position);
        viewHolder.asignarDatos(categoria);
    }

    @Override
    public int getItemCount() {
        return listaCategorias.size();
    }

    //************************************************************
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txt_categoria_lista;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_categoria_lista=itemView.findViewById(R.id.txt_categoria_lista);
        }
        public void asignarDatos(Categoria categoria){
            txt_categoria_lista.setText(categoria.getNombre());
        }
    }//endholder
}//end
