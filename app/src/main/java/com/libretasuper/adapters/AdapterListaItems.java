package com.libretasuper.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.libretasuper.R;
import com.libretasuper.modelo.Item;

import java.util.ArrayList;


public class AdapterListaItems extends RecyclerView.Adapter<AdapterListaItems.ViewHolder>
        implements View.OnClickListener {
    //var
    ArrayList<Item> listaItems;

    View.OnClickListener listener;

    public AdapterListaItems(ArrayList<Item> lista){
        this.listaItems=lista;
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lista_items,null,false);

        view.setOnClickListener(this);

        ViewHolder holder=new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Item item=listaItems.get(position);
        viewHolder.asignarDatos(item);
    }

    @Override
    public int getItemCount() {
        return listaItems.size();
    }

    @Override
    public void onClick(View view) {
        if(listener!=null){
            listener.onClick(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txt_item_lista;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_item_lista=(TextView)itemView.findViewById(R.id.txt_item_lista);
        }
        public void asignarDatos(Item item){
            txt_item_lista.setText(item.getNombre());
        }
    }//end holder
}//end
