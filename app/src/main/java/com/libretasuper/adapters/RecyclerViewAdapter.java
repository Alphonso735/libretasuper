package com.libretasuper.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.libretasuper.R;
import com.libretasuper.modelo.Item;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>
    implements View.OnClickListener
{
    private static final String TAG = "RecyclerViewAdapter";
    //***************************************************************
    private ArrayList<Item> listaItems=new ArrayList<>();

    //----------------------
    private View.OnClickListener listener;

    //***************************************************************

    public RecyclerViewAdapter( ArrayList<Item> listaItems) {
        this.listaItems = listaItems;

    }

    //***************************************************************
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_list_item, viewGroup, false);

        ViewHolder holder=new ViewHolder(view);

        view.setOnClickListener(this);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        Log.d(TAG, "onBindViewHolder: Llamado.");
        //************************
        Item item=listaItems.get(i);

        viewHolder.txtItemList.setText(item.getNombre());
        viewHolder.txtCantidad.setText(String.valueOf(item.getCantidad()));
        //************************
       /* viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: "+listaItems.get(i).getNombre());

                Toast.makeText(context,listaItems.get(i).getNombre(),Toast.LENGTH_SHORT).show();
            }
        });
        */
    }

    @Override
    public int getItemCount() {
        return listaItems.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.onClick(v);
        }
    }


    //*********************************************************
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtItemList,txtCantidad;
        RelativeLayout parentLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItemList=itemView.findViewById(R.id.list_item);
            txtCantidad=itemView.findViewById(R.id.list_item_cantidad);
            parentLayout=itemView.findViewById(R.id.parent_layout);
        }
    }//endclass ViewHolder
}//end
