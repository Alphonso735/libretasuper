package com.libretasuper.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.strictmode.SqliteObjectLeakedViolation;

import com.libretasuper.database.utilidades.Utilidades;

import java.io.Serializable;

public class ConexionSQLiteHelper extends SQLiteOpenHelper implements Serializable {

    public ConexionSQLiteHelper(Context context,String name,  SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Utilidades.CREAR_TABLA_CATEGORIAS);
        db.execSQL(Utilidades.CREAR_TABLA_ITEMS);
        llenarBD(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionVieja, int versionNueva) {
       borrarBD(db);
       onCreate(db);
    }
    private void borrarBD(SQLiteDatabase db){
        db.execSQL("DROP TABLE IF EXISTS "+Utilidades.TABLA_CATEGORIAS);
        db.execSQL("DROP TABLE IF EXISTS "+Utilidades.TABLA_ITEMS);
    }
    public void onRestart(){

       borrarBD(this.getWritableDatabase());
       onCreate(this.getWritableDatabase());
    }

    private void llenarBD(SQLiteDatabase db){
        //----Categorias--------
        String[] categorias={"Alimentos","Bebidas","Limpieza","Limpieza personal"};
        long[] idCat=new long[4];

        for(int i=0;i<categorias.length;i++){
            ContentValues cv=new ContentValues();
            cv.put(Utilidades.CAT_NOMBRE,categorias[i]);

            idCat[i]=db.insert(Utilidades.TABLA_CATEGORIAS,Utilidades.CAT_ID,cv);

            cv.clear();
        }
        //------------------Items-----------------------------
        String[] items1={"Arroz","Fideos","Fid. Cab. Angel","Lentejas","Harina","Arroz Integral","Sal",
            "Edulcorante","Azucar","Huevos","Queso Cremoso","Queso","Mate Cocido","Soja","Arbejas"};

        for(String item : items1){
            ContentValues cv=new ContentValues();
            cv.put(Utilidades.IT_NOMBRE,item);
            cv.put(Utilidades.IT_CANTIDAD,0);
            cv.put(Utilidades.IT_CATEGORIA,1);

            db.insert(Utilidades.TABLA_ITEMS,Utilidades.IT_ID,cv);
            cv.clear();
        }

        //--------------------------------------

        String[] items2={"Cerveza","Cerveza Negra","Jugo Cligh","Vino","Coca-Cola","Agua Mineral",
                "Fanta","Pack Cerveza"};

        for(String item : items2){
            ContentValues cv=new ContentValues();
            cv.put(Utilidades.IT_NOMBRE,item);
            cv.put(Utilidades.IT_CANTIDAD,0);
            cv.put(Utilidades.IT_CATEGORIA,2);

            db.insert(Utilidades.TABLA_ITEMS,Utilidades.IT_ID,cv);
            cv.clear();
        }
        //---------------------------------------------
        String[] items3={"Esponja","Detergente","Ciff","Birulana","Trapo de piso","Lavandina",
                "Jabon Liquido","Suavisante","Desengrasante","Escoba","Lampazo"};

        for(String item : items3){
            ContentValues cv=new ContentValues();
            cv.put(Utilidades.IT_NOMBRE,item);
            cv.put(Utilidades.IT_CANTIDAD,0);
            cv.put(Utilidades.IT_CATEGORIA,3);

            db.insert(Utilidades.TABLA_ITEMS,Utilidades.IT_ID,cv);
            cv.clear();
        }
        //---------------------------------------------------
        String[] items4={"Talco","Desodorante","Colonia","Peine","Pasta de dientes","Maquina de afeitar",
            };

        for(String item : items4){
            ContentValues cv=new ContentValues();
            cv.put(Utilidades.IT_NOMBRE,item);
            cv.put(Utilidades.IT_CANTIDAD,0);
            cv.put(Utilidades.IT_CATEGORIA,4);

            db.insert(Utilidades.TABLA_ITEMS,Utilidades.IT_ID,cv);
            cv.clear();
        }
        //--------------------------------------------------------
    }
}//end
