package com.libretasuper.database;

import android.content.Context;


import com.libretasuper.database.repository.CategoriaRepository;
import com.libretasuper.database.repository.ItemRepository;
import com.libretasuper.modelo.Categoria;
import com.libretasuper.modelo.Item;
import com.libretasuper.database.utilidades.Utilidades;

import java.io.Serializable;

import java.util.ArrayList;

public class DBHandler implements Serializable {
    //var
    private ItemRepository itemRepo;
    private CategoriaRepository categoriaRepo;

    private ConexionSQLiteHelper conexion;

    public DBHandler(Context context){
        this.conexion=new ConexionSQLiteHelper(context,Utilidades.DATABASE,null,Utilidades.DATABASE_VERSION);
        this.itemRepo=new ItemRepository(context);
        this.categoriaRepo=new CategoriaRepository(context);
    }

    //-------------Categorias---------------------------
    public boolean insertarCategoria(Categoria categoria){
       return categoriaRepo.insertar(categoria);
    }

    public ArrayList<Categoria> listaCategorias(){

       return categoriaRepo.obtenerListCat();
    }

    //------------Items----------------------------------
    public boolean insertarItem(Item item){
        boolean result=itemRepo.insertar(item);
        return result;
    }

    //lista de items en lista
    public ArrayList<Item> listaItems(){

        return itemRepo.obtenerItemConCantidad();
    }

    //lista completa de items
    public ArrayList<Item> listaCompletaItems(){
       return itemRepo.obtenerListItems();
    }

    public ArrayList<Item> listaItemsFromCategoria(Categoria categoria) {
        return itemRepo.listaSegunCategoria(categoria);
    }

    public boolean borrarItemLista(Item itemSelect) {
        itemSelect.setCantidad(0);
        return itemRepo.actualizar(itemSelect);
    }

    public boolean addItemToList(Item itemSelect) {
        return itemRepo.actualizar(itemSelect);
    }

    public boolean borrarItem(Item item){
        return itemRepo.borrar(item);
    }

    public boolean borrarCategoria(Categoria categoria){
        boolean result=false;
        //----------------------
        ArrayList<Item> listaItems=itemRepo.listaSegunCategoria(categoria);

        for(Item item:listaItems){
            this.borrarItem(item);
        }
        result=categoriaRepo.borrar(categoria);
        //----------------------
        return  result;
    }

    public void cerrar(){
        this.conexion.close();
    }

    public void resetearBD(){
        this.conexion.onRestart();
    }
}//end
