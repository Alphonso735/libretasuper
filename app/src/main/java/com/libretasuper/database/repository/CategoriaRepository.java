package com.libretasuper.database.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.libretasuper.database.ConexionSQLiteHelper;
import com.libretasuper.database.utilidades.Utilidades;
import com.libretasuper.modelo.Categoria;
import com.libretasuper.modelo.Entidad;

import java.util.ArrayList;

public class CategoriaRepository extends Repository implements IRepository {

    //constructor
    public CategoriaRepository(Context context){
        super.conexion=new ConexionSQLiteHelper(context, Utilidades.DATABASE,null,Utilidades.DATABASE_VERSION);
    }

    public ArrayList<Categoria> obtenerListCat(){
        ArrayList<Categoria> result=new ArrayList<>();
        //---------------------------------
        SQLiteDatabase db=conexion.getReadableDatabase();

        String[]campos={Utilidades.CAT_ID,Utilidades.CAT_NOMBRE};

        Cursor cursor=db.query(Utilidades.TABLA_CATEGORIAS,campos,null,null,
                null,null,null);

        while(cursor.moveToNext()){
            result.add(new Categoria(
                    cursor.getInt(0),
                    cursor.getString(1)
            ));
        }
        cursor.close();
        db.close();
        //---------------------------------
        return result;
    }


    //****************************************************************
    @Override
    public boolean insertar(Entidad entidad) {
        boolean result=false;
        //-------------------------------------
        if(entidad instanceof Categoria){
            Categoria categoria=(Categoria)entidad;
           if(!this.existe(categoria)){
               //---------------------------
               SQLiteDatabase db=super.conexion.getWritableDatabase();

               ContentValues cv=new ContentValues();
               cv.put(Utilidades.CAT_NOMBRE,categoria.getNombre());

               long rs=db.insert(Utilidades.TABLA_CATEGORIAS,Utilidades.CAT_ID,cv);

               db.close();
               //---------------------------
               result=(rs>0L)?true:false;
           }
        }
        //-------------------------------------
        return result;
    }

    @Override
    public boolean borrar(Entidad entidad) {
        boolean result=false;
        //---------------------------------
        if(entidad instanceof Categoria){
            Categoria categoria=(Categoria)entidad;
            //**********************************
            SQLiteDatabase db=super.conexion.getWritableDatabase();

            String[] parametros={String.valueOf(categoria.getId())};

            int rs=db.delete(Utilidades.TABLA_CATEGORIAS,Utilidades.CAT_ID+"=?",parametros);

            db.close();
            //**********************************
            result=(rs>0)?true:false;
        }
        //---------------------------------
        return result;
    }

    @Override
    public boolean actualizar(Entidad entidad) {
        boolean result=false;
        //---------------------------------
        if(entidad instanceof Categoria){
            Categoria categoria=(Categoria)entidad;
            //******************************
            SQLiteDatabase db=super.conexion.getWritableDatabase();

            ContentValues cv=new ContentValues();
            cv.put(Utilidades.CAT_NOMBRE,categoria.getNombre());

            String[] parametros={String.valueOf(categoria.getId())};

            int rs=db.update(Utilidades.TABLA_CATEGORIAS,cv,Utilidades.CAT_ID+"=?",parametros);

            db.close();
            //******************************
            result=(rs>0)?true:false;
        }
        //---------------------------------
        return result;
    }

    @Override
    public boolean existe(Entidad entidad) {

        boolean result=false;
        //---------------------------------
        if(entidad instanceof Categoria){
            Categoria categoria=(Categoria)entidad;
            //******************************
            SQLiteDatabase db=super.conexion.getReadableDatabase();

            String[] campos={Utilidades.CAT_ID,Utilidades.CAT_NOMBRE};
            String[] parametros={String.valueOf(categoria.getId())};

            Cursor cursor=db.query(Utilidades.TABLA_CATEGORIAS,campos,Utilidades.CAT_ID+"=?",parametros,
                    null,null,null);

            //******************************
            try{
                result=(cursor.moveToFirst())?true:false;
            }catch (Exception e){
                result=false;
            }

            cursor.close();
            db.close();
        }
        System.out.println("Categoria existe result="+result);
        //---------------------------------
        return result;
    }

    @Override
    public Entidad obtener(int id) {
        Entidad result=null;
        //--------------------------
        SQLiteDatabase db=conexion.getReadableDatabase();

        String[] campos={Utilidades.CAT_ID,Utilidades.CAT_NOMBRE};
        String[] parametros={String.valueOf(id)};

        Cursor cursor=db.query(Utilidades.TABLA_CATEGORIAS,campos,Utilidades.CAT_ID+"=?",parametros,
                null,null,null);

        if(cursor.moveToFirst()){
            result=new Categoria(
                    cursor.getInt(0),
                    cursor.getString(1)
            );

         cursor.close();
         db.close();
        }
        //--------------------------
        return result;
    }
}
