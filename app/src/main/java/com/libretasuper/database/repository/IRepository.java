package com.libretasuper.database.repository;

import com.libretasuper.modelo.Entidad;

public interface IRepository {

    public boolean insertar(Entidad entidad);
    public boolean borrar(Entidad entidad);
    public boolean actualizar(Entidad entidad);
    public boolean existe(Entidad entidad);
    public Entidad obtener(int id);
}
