package com.libretasuper.database.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.nfc.Tag;
import android.util.Log;

import com.libretasuper.database.ConexionSQLiteHelper;
import com.libretasuper.database.utilidades.Utilidades;
import com.libretasuper.modelo.Categoria;
import com.libretasuper.modelo.Entidad;
import com.libretasuper.modelo.Item;

import java.util.ArrayList;

public class ItemRepository extends Repository implements IRepository {
    //var
    private CategoriaRepository catRepo;
     //constructor
     public ItemRepository(Context context){
         super.conexion=new ConexionSQLiteHelper(context, Utilidades.DATABASE,null,Utilidades.DATABASE_VERSION);
         //-------------------------------------------
         this.catRepo=new CategoriaRepository(context);
     }
    //all items
     public ArrayList<Item> obtenerListItems(){
         ArrayList<Item> result=new ArrayList<>();
         //--------------------------
         SQLiteDatabase db=conexion.getReadableDatabase();

         String[] campos={Utilidades.IT_ID,Utilidades.IT_NOMBRE,Utilidades.IT_CANTIDAD,Utilidades.IT_CATEGORIA};

         Cursor cursor=db.query(Utilidades.TABLA_ITEMS,campos,null,null,
                 null,null,null);

         while(cursor.moveToNext()){
             Categoria categoria=(Categoria) catRepo.obtener(cursor.getInt(3));
             result.add(new Item(
                     cursor.getString(1),
                     cursor.getInt(2),
                     cursor.getInt(0),
                     categoria
             ));
         }
         cursor.close();
         db.close();
         //--------------------------
         return result;
     }

     //items list
    public ArrayList<Item> obtenerItemConCantidad(){
        ArrayList<Item> result=new ArrayList<>();
        //------------------------
        SQLiteDatabase db=conexion.getReadableDatabase();

        String[] campos={Utilidades.IT_ID,Utilidades.IT_NOMBRE,Utilidades.IT_CANTIDAD,Utilidades.IT_CATEGORIA};
        String[] parametros={"0"};

        Cursor cursor=db.query(Utilidades.TABLA_ITEMS,campos,Utilidades.IT_CANTIDAD+">?",parametros,
                null,null,null);

        while(cursor.moveToNext()){
            Categoria categoria=(Categoria) catRepo.obtener(cursor.getInt(3));
            result.add(new Item(
                    cursor.getString(1),
                    cursor.getInt(2),
                    cursor.getInt(0),
                    categoria
            ));
        }
        cursor.close();
        db.close();
        //------------------------
        return result;
    }

    //lista de items segun una categoria
    public ArrayList<Item> listaSegunCategoria(Categoria categoria){
        ArrayList<Item> result=new ArrayList<>();
        //---------------------------
        SQLiteDatabase db=conexion.getReadableDatabase();

        String[] campos={Utilidades.IT_ID,Utilidades.IT_NOMBRE,Utilidades.IT_CANTIDAD,Utilidades.IT_CATEGORIA};

        String[] parametos={String.valueOf(categoria.getId())};

        Cursor cursor=db.query(Utilidades.TABLA_ITEMS,campos,Utilidades.IT_CATEGORIA+"=?",parametos,
                null,null,null);

        while(cursor.moveToNext()){
            result.add(new Item(
                    cursor.getString(1),
                    cursor.getInt(2),
                    cursor.getInt(0),
                    categoria
            ));
        }
        cursor.close();
        db.close();

        //---------------------------
        return result;
    }
     //***********************************************************************
    @Override
    public boolean insertar(Entidad entidad) {

         if(entidad instanceof Item){
             Item item=(Item)entidad;
             if(!this.existe(item)){

                 if(catRepo.existe(item.getCategoria())){
                     SQLiteDatabase db=super.conexion.getWritableDatabase();
                     //-------------------------------------------------
                     ContentValues values=new ContentValues();
                     values.put(Utilidades.IT_NOMBRE,item.getNombre());
                     values.put(Utilidades.IT_CANTIDAD,item.getCantidad());
                     values.put(Utilidades.IT_CATEGORIA,item.getCategoria().getId());

                     Long result=db.insert(Utilidades.TABLA_ITEMS,Utilidades.IT_ID,values);
                     //-------------------------------------------------
                     values.clear();
                     db.close();
                     //-------------

                     if(result>0L)
                         return true;
                     else
                         return false;

                 }//endcatexists

             }//enditemexists

         }
         return false;
    }

    @Override
    public boolean borrar(Entidad entidad) {
         boolean result=false;
        if(entidad instanceof Item){

            Item item=(Item)entidad;

            SQLiteDatabase db=this.conexion.getWritableDatabase();
            String[] parametros={String.valueOf(item.getId())};

            if(db.delete(Utilidades.TABLA_ITEMS,Utilidades.IT_ID+"=?",parametros)>0)
                  result=true;

            db.close();
        }
        return result;
    }

    @Override
    public boolean actualizar(Entidad entidad) {
        boolean result=false;
        //-----------------------------
        if(entidad instanceof Item){

            Item item=(Item)entidad;

            SQLiteDatabase db=this.conexion.getWritableDatabase();

            String[] parametros={String.valueOf(item.getId())};

            ContentValues cv=new ContentValues();
            cv.put(Utilidades.IT_NOMBRE,item.getNombre());
            cv.put(Utilidades.IT_CANTIDAD,item.getCantidad());
            cv.put(Utilidades.IT_CATEGORIA,item.getCategoria().getId());

            if(db.update(Utilidades.TABLA_ITEMS,cv,Utilidades.IT_ID+"=?",parametros)>0)
                result=true;

            db.close();
        }
        //-----------------------------

        return result;
    }

    @Override
    public boolean existe(Entidad entidad) {

         boolean result=false;

         if(entidad instanceof Item){
             Item item=(Item)entidad;

             SQLiteDatabase db=this.conexion.getReadableDatabase();

             long idItem=item.getId();

             String[] campos={Utilidades.IT_ID};
             String[] parametros={String.valueOf(idItem)};

             Cursor cursor=db.query(Utilidades.TABLA_ITEMS,campos,Utilidades.IT_ID+"=?",parametros,
                     null,null,null);

             try{
                 if(cursor.moveToFirst())
                     result= true;
                 else
                     result= false;
             }catch(Exception e){
                 result=false;
             }

             cursor.close();
             db.close();
         }//endinstanceof

        return result;
    }

    @Override
    public Entidad obtener(int id) {
        Entidad result=null;
        //-----------------------
        SQLiteDatabase db=conexion.getReadableDatabase();

        String[] campos={Utilidades.IT_ID,Utilidades.IT_NOMBRE,Utilidades.IT_CANTIDAD,Utilidades.IT_CATEGORIA};
        String[] parametros={String.valueOf(id)};

        Cursor cursor=db.query(Utilidades.TABLA_ITEMS,campos,Utilidades.IT_ID+"=?",parametros,
                null,null,null);

        if(cursor.moveToFirst()){

            Categoria categoria=(Categoria)catRepo.obtener(cursor.getInt(3));

            result=new Item(cursor.getString(1),
                    cursor.getInt(2),
                    cursor.getInt(0),
                   categoria);

        }

        cursor.close();
        db.close();
        //-----------------------
        return result;
    }
}
