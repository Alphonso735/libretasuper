package com.libretasuper.database.utilidades;

public class Utilidades {
    //database
    public static final String DATABASE="db_LibretaSuper";
    public static final int DATABASE_VERSION=1;
    //tabla categorias
    public static final String TABLA_CATEGORIAS="categorias";
    public static final String CAT_ID="id";
    public static final String CAT_NOMBRE="nombre";

    public static final String CREAR_TABLA_CATEGORIAS="CREATE TABLE IF NOT EXISTS "+TABLA_CATEGORIAS+
            " ("+CAT_ID+" INTEGER PRIMARY KEY, "+CAT_NOMBRE+" TEXT)";

    //tabla items
    public static final String TABLA_ITEMS="items";
    public static final String IT_ID="id";
    public static final String IT_NOMBRE="nombre";
    public static final String IT_CANTIDAD="cantidad";
    public static final String IT_CATEGORIA="idCategoria";

    public static final String CREAR_TABLA_ITEMS="CREATE TABLE IF NOT EXISTS "+TABLA_ITEMS+
            " ("+IT_ID+" INTEGER PRIMARY KEY, "+IT_NOMBRE+" TEXT, "+IT_CANTIDAD+" INTEGER, "+IT_CATEGORIA+" INTEGER,"+
            "FOREIGN KEY ("+IT_CATEGORIA+") REFERENCES "+TABLA_CATEGORIAS+" ("+CAT_ID+"))";


}//end
