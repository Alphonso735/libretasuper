package com.libretasuper.modelo;

public class Categoria extends Entidad{

    public Categoria(int id, String nombre) {
        super(id, nombre);
    }
    public Categoria(String nombre){
        super(0,nombre);
    }

    @Override
    public String toString() {
        return super.getNombre();
    }
}//end
