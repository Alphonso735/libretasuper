package com.libretasuper.modelo;

public class Item extends Entidad{

    private int cantidad;
    private Categoria categoria;

    public Item(String nombre, int cantidad, Categoria categoria) {
        super(0,nombre);
        this.categoria = categoria;
        this.cantidad=cantidad;
    }

    public Item(String nombre, int cantidad, int id, Categoria categoria) {
        super(id,nombre);
        this.cantidad=cantidad;
        this.categoria = categoria;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @Override
    public String toString() {
        return super.getNombre();
    }
}//end
